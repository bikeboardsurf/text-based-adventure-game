class ConfigurationException(Exception): 
    def __init__(self, value): 
        self.value = value
    def __str__(self): 
        return "Error: %s" % self.value
try:
    raise ConfigurationException("Could not load or process configuration setting correctly")
except ConfigurationException as e:
    print(e)