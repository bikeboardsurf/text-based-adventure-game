## Text based adventure game
> An ancient, magical but dangerous land
<img src='media/pictures/wizardHat.png' width='30%'/>

## Why does this game exist?
I created this game to get more familiar with Python and to encourage younger members of my family to practice using the keyboard.

### Object of the game
You are a chosen hero who needs to prepare for a battle against an evil being that has been terrorising the villagers. Find friends that assist in your readiness to fight. On the way there will be a number of challenges of wit and luck, will you prosper and win? You can gather a weapon, some protection such as a helmet and a health potion which will automatically be administered. The weapon and protection varies depending what is issued (there is at least 1 dud). Each weapon has a hit point rating, this will boost your hit rating when attacking the monster at the end of the game (which you need to find). Similarly the protection that you get will range depending what is issued. You will automatically start using your protective item when in a fight and your health reaches a level below a specific threshold. It will deduct the attackers hit points and therefore reduce its impact on you. 

Once the game is won, you are given a secret code, this can be used in real life attached to something like a combination lock, the revealed code can be configured to match the combination to reveal the contents.

### Moving around the map
The player can move around by inputting 'n', 'e', 's' or 'w' when prompted, these stand for north, east, south or west respectively. The code has controls to ensure the player does not move beyond the map/2d boundary

### Technical overview
The game map consists of a 2d array, the player moves across and up and down using supported keyboard input keys. Each item in the 2d array consists of a MapSquare object. The MapSquare object can hold a 'sprite' or 'being' (such as a villager or a wizard), an 'object' (such as a weapon or a physical item) and a pointer to a callback method which returns True or False. The idea is that the sprite will ask if the player would like to participate in a challenge (the challenge is defined in the callback), if the challenge is successful the player gains the object in that square. Not all MapSquares need to have an object/sprite/callback however each one should have a description. When the player moves around they see the description associated with the MapSquare. There are two types of description - full description and a short description, if the player has already visited the map square then they see they automatically get the shortened version.

#### Configuration 
All the game configuration is kept in the configuration/ directory. The configuration.json relates to the physical running of the application, currently the log level. The game-configuration is general game related configuration such as difficulty level, 'God Mode' etc. All other configuration relates to the customising of the sub game challenges.

### Sub Games
Within the game there are 'sub games', these are smaller challenges which, if completed successfully would mean the player gains something (such as a weapon, protection or additional health). Sub games can be easily added in the logistics/subgames.py file. The sub game needs to follow the pattern of a function with no parameters it must and return true or false depending if the challenge was successful or not. The sub game function serves as a callback, when the player lands on the map square that has a callback the challenge will be initiated. This setup makes the sub games easily extensible.

### Prerequisites
The game requires Python 3

### Running the game
The object oriented version use 'python startgame.py'

### Features

#### Weapons
During the game you will have the ability to win a weapon. The weapon varies from game to game, this will change how effective it is. Even if a powerful weapon is obtained, how effective it will be in battle will range from a miss to the maximum hit rating of the weapon

#### Protection
You will have the ability to win a protective item which varies from game to game. You will only use the protective item when in battle and your health points are below a threshold. The protection is automatically applied

#### Health Potion
Once successful in winning a health potion, during a battle you will automatically drink the potion giving you extra health. You will only drink the potion when its needed.

## Enemies

### Toad
Image coming soon

### Ogre
Image coming soon

### Banchee
Image coming soon

### Kraken
Image coming soon

### Cerberus
Image coming soon

## Weapons

### Axe
Image coming soon

### Sword
Image coming soon