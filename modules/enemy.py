from modules.thing import Thing
class Enemy(Thing):
   'Common base class for all enemies'
   def __init__(self, name, description, weapon, health):
      Thing.__init__(self, name, description)
      self.weapon = weapon
      self.health = health

   def __str__(self):
        return "The creature is a "+self.name+". "+self.description+".\nIts weapon is "+str(self.weapon)+", the creature has an initial health of "+str(self.health)+"."

   def get_weapon(self):
       return self.weapon

   def get_health(self):
       return self.health
    
   def deduct_health(self, deduction):
       self.health = self.health - deduction
       return self.health


