from modules.thing import Thing
class Friend(Thing):
   #Base class for friendly beings e.g. villager and magic being, these do not carry weapons
    def __init__(self, name, description, greeting, beingtype):
        Thing.__init__(self, name, description)
        self.greeting = greeting
        self.beingtype = beingtype
  
    def __str__(self):
        return "You see "+self.beingtype+" called "+self.name+" who "+self.description+".\n'"+self.greeting+"'"

    def get_greeting(self):
        return self.greeting
    
    def get_being_type(self):
        return self.beingtype