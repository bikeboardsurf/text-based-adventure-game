import logging
from modules.villager import Villager
from modules.magicbeing import MagicBeing
from modules.mapsquareobject import MapSquareObject
import logging
import constants

logging.getLogger(constants.LOGGER_NAME)
logging.basicConfig(level=logging.DEBUG)

class MapSquare:
    #'Common base class for all map squares'
    def __init__(self, short_description, long_description):
        self.short_description = short_description
        self.long_description = long_description
        self.visited = False
        self.item = None
        self.being = None
        self.locked = False
        self.locked_description = None
        self.lock_key_required = None
        self.locked = False
       
    def __str__(self):
       item_output = ""
       if self.item is not None:
           item_output = "You can see "+str(self.item)+".\n"
       #else:
        #  logging.debug("Map Square is empty")
       if self.being is not None:
          item_output += str(self.being)
       if (self.visited):
           return self.short_description+". "+item_output
       else:
           return self.long_description+". "+item_output

    def set_visited(self, visited):
       self.visited = visited

    def set_item(self, item):
       self.item = item

    def set_locked(self, locked):
        self.locked = locked

    def set_key_required_to_unlock(self, lock_key_required_to_unlock):
        self.lock_key_required = lock_key_required_to_unlock

    def set_locked_description(self, locked_description):
        self.locked_description = locked_description
      
    #holds a wizard, villager, enemy etc (a living being)
    def set_being(self, being):
        self.being = being

    def set_callback(self, callback_function):
        self.callback = callback_function

    def get_visited(self):
       return self.visited

    def get_short_description(self):
       return self.short_description
    
    def get_set_lock_key_required_to_unlock(self):
        return self.lock_key_required

    def get_long_description(self):
       return self.long_description

    def get_item(self):
       return self.item
    
    def get_callback(self):
        return self.callback

    def get_being(self):
        return self.being
    
    def is_locked(self):
        return self.locked
    
    def get_locked_description(self):
        return self.locked_description