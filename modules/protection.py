from modules.thing import Thing
class Protection(Thing):
   #Common base class for all protective items
   def __init__(self, name, description, protectionPoints):
      Thing.__init__(self, name, description)
      self.protectionPoints = protectionPoints

   def __str__(self):
        return self.name+", "+self.description+". It has a protection point rating of +"+str(self.protectionPoints)

   def get_protection_points(self):
       return self.protectionPoints
