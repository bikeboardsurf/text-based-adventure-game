from modules.thing import Thing
class Weapon(Thing):
   #Common base class for all weapons
   def __init__(self, name, description, hitPoints):
      Thing.__init__(self, name, description)
      self.hitPoints = hitPoints

   def __str__(self):
        return ""+self.name+", "+self.description+". The weapon has hit point rating of +"+str(self.hitPoints)

   def get_hit_points(self):
       return self.hitPoints
