from modules.weapon import Weapon
from modules.protection import Protection
from modules.thing import Thing
from modules.healthpotion import HealthPotion
from modules.carryitem import CarryItem

class Player(Thing):
   #Common base class for all players
    def __init__(self, name, description, health, weapon, protection):
        Thing.__init__(self, name, description)
        self.health = health
        self.weapon = weapon
        self.protection = protection
        self.god_mode = False
        self.healthPotion = None
        self.carrying_items = []

    def __str__(self):
        carrying_weapon = "You are not carrying any weapon, you MUST arm yourself"
        if self.weapon is not None:
            carrying_weapon = str(self.weapon)
        carrying_protection = "You are not carrying anything to protect yourself, find something, anything."
        if self.protection is not None:
            carrying_protection = str(self.protection)
        output = "You are warrior "+self.name+" the Brave.\n"+self.description+".\nYou have health of "+str(self.health)+".\n"+carrying_weapon+". "+carrying_protection
        return output

    def set_weapon(self, weapon):
        self.weapon = weapon

    def set_protection(self, protection):
        self.protection = protection
    
    def set_god_mode(self, godmode):
        self.god_mode = godmode

    def set_health_potion(self, healthPotion):
        self.healthPotion = healthPotion

    def get_weapon(self):
        return self.weapon
    
    def get_god_mode(self):
        return self.god_mode

    def get_health(self):
        return self.health
    
    def get_protection(self):
        return self.protection
    
    def get_health_potion(self):
        return self.healthPotion

    def deduct_health(self, deduction):
        self.health = self.health - deduction
        return self.health
    
    def add_health(self, addition):
        self.health = self.health + addition
        return self.health
    
    def add_carry_item(self, carry_item):
        self.carrying_items.append(carry_item)
    
    def get_carry_items(self):
        return self.carrying_items
