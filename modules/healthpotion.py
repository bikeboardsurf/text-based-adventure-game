from modules.thing import Thing
class HealthPotion(Thing):
   def __init__(self, name, description, healthPoints):
      Thing.__init__(self, name, description)
      self.healthPoints = healthPoints

   def __str__(self):
        return self.name+"."+self.description+". If the potion is drunk, it will offer additional health of +"+str(self.healthPoints)

   def get_health_points(self):
       return self.healthPoints
