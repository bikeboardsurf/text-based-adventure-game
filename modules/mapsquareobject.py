class MapSquareObject:
   'Common base class for all map square objects'
   def __init__(self, name, description):
      self.name = name
      self.description = description

   def __str__(self):
        return "You see "+self.name+". " +self.description

   def get_name(self):
       return self.name

   def get_description(self):
       return self.description
