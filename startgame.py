#!/usr/bin/python
from logistics.game import Game
from customexceptions.configurationexception import ConfigurationException
game = Game()
try:
    game.start_game()
except ConfigurationException:
    print(
        "A critical error has occurred: " +
        "could not load the game configuration")
except Exception:
    print(
        "A critical issue has occurred " +
        "with the game and it had to be terminated")
