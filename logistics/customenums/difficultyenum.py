from enum import Enum, unique
@unique
class Difficulty(Enum):
    EASY = "EASY"
    MEDIUM = "MEDIUM"
    HARD = "HARD"
    #print(member.name)
    #print(member.value)