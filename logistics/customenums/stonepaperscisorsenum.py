from enum import Enum, unique
@unique
class SPSSelection(Enum):
    STONE = 1
    PAPER = 2
    SCISSORS = 3
    #print(member.name)
    #print(member.value)