from enum import Enum, unique
from logging import NOTSET
@unique
class LoggingLevel(Enum):
    NOTSET = "NOTSET"
    DEBUG = "DEBUG"
    INFO = "INFO"
    WARNING = "WARNING"
    ERROR = "ERROR"
    CRITICAL = "CRITICAL"
    #print(member.name)
    #print(member.value)