from pickle import FALSE
from random import randint
import random
from logistics.logger import Logger
import utilities
from logistics.customenums.stonepaperscisorsenum import SPSSelection
from logistics.configurationloader import ConfigurationLoader
import constants
import sys
import select


def stone_paper_paper_scissors():
    utilities.clear_screen()
    print("Stone, Paper, Scissors game. First to win three times wins!")
    playerscore = 0
    computerscore = 0

    while playerscore < 3 and computerscore < 3:
        print("1 for Stone")
        print("2 for Paper")
        print("3 for Scissors")

        playersinput = 0
        playersinput_enummapped = ""
        while (playersinput == 0):
            try:
                playersinput = int(utilities.user_input("", True))
                playersinput_enummapped = SPSSelection(playersinput).name
            except ValueError as ve:
                print(str(ve))
                print("Invalid input please enter a corresponding number")
                playersinput = 0

        computerinput = randint(1, 3)
        print("Your selection is "+playersinput_enummapped)
        print(
            "Your challenger's selection is "
            + SPSSelection(computerinput).name)
        if playersinput == computerinput:
            print("Draw")
        elif playersinput == SPSSelection.STONE.value:
            if computerinput == SPSSelection.SCISSORS.value:
                print("You win this round")
                playerscore = playerscore + 1
            else:
                print("Challenger wins this round")
                computerscore = computerscore + 1
        elif playersinput == SPSSelection.PAPER.value:
            if computerinput == SPSSelection.STONE.value:
                print("You win this round")
                playerscore = playerscore + 1
            else:
                print("Challenger wins this round")
                computerscore = computerscore + 1
        elif playersinput == SPSSelection.SCISSORS.value:
            if computerinput == SPSSelection.PAPER.value:
                print("You win this round")
                playerscore = playerscore + 1
            else:
                print("Challenger wins this round")
                computerscore = computerscore + 1
        print(
            "Your score is " + str(playerscore) + ", your " +
            "challenger's score is " + str(computerscore)+"\n")
        if playerscore == 3 or computerscore == 3:
            break
    if playerscore > computerscore:
        print("You won!!!")
        return True
    else:
        print("You lost!!")
    return False


def riddle():
    utilities.clear_screen()
    print(
        "Riddle me this, riddle me that, my hard riddles " +
        "will make you fall flat\n")
    riddles = ConfigurationLoader.instance().get_riddles()
    questions = list(riddles.keys())
    random.shuffle(questions)
    question = questions[0]
    Logger.instance().debug("Question found:  "+question)
    answer = riddles[question]
    Logger.instance().debug("Answer is " + answer)
    try:
        userAnswer = utilities.user_input(question+"\n", True)
        if userAnswer.strip().lower().find(answer.lower()) > -1:
            Logger.instance().debug("Riddle question correct")
            # Do not want to reuse the same question next time so
            # remove it if there is more than one riddle remaining
            if len(riddles) > 1:
                Logger.instance().debug(
                    "Popping riddle as it has been used")
                riddles.pop(question)
            return True
    except Exception as err:
        Logger.instance().error(str(err))
    return False

def maths():
    utilities.clear_screen()
    timer = ConfigurationLoader.instance().get_maths_timer()
    print(
        "I hope you pay attention in maths class, let's find out." +
        "But be quick, I am impatient, you have "+str(timer) +" seconds "+
        "to answer each maths question.\n"
    )
    utilities.user_input("Press enter to continue", False)
    operators = ["+", "-"]
    l = 0
    maxLoop = 5
    highestRandomNumber = ConfigurationLoader.instance().get_maths_max_numer()
    timer = ConfigurationLoader.instance().get_maths_timer()
    success = False
    while (l < maxLoop):
        random1 = random.randint(1,highestRandomNumber)
        random2 = random.randint(1,highestRandomNumber)
        chosenOperator = random.choice(operators)
        numberSentence = "%s%s%s"%(random1, chosenOperator, random2)
        #Note- be careful with eval, its a security hazard
        answer=eval(numberSentence)
        print ("What does %s %s %s = ?"%(random1, chosenOperator, random2))
        i, o, e = select.select([sys.stdin], [], [], timer)
        if (i):
            user_input = utilities.user_input("", True)
            if(int(user_input) == int(answer)):
                success = True
                print(
                    "You answered correctly")
                Logger.instance().debug("Maths question correct")
            else:
                print(
                    "You answered INCORRECTLY, better luck next time")
                Logger.instance().debug("Maths question INCORRECT")
                success = False
                break
        else:
            print(
                "You ran out of time")
            Logger.instance().debug("Maths question - out of time")
            success = False
            break
        l = l + 1 
    return success