from logistics.customenums.difficultyenum import Difficulty
from modules.enemy import Enemy
from modules.weapon import Weapon
from random import *
from logistics.logger import Logger
from logistics.configurationloader import ConfigurationLoader


def generate_ogre():
    return Enemy(
        "Orgre",
        "It looks like an old wrinkly avocado, but smells a lot worse",
        Weapon("a club", "a cold, solid piece of tree", randint(1, 5)),
        randint(30, 45))


def generate_kraken():
    return Enemy(
        "Kraken",
        "It is big with a giant beak and eight tenticles",
        Weapon("a sharp beak", "point like needle", randint(5, 10)),
        randint(45, 55))


def generate_toad():
    return Enemy(
        "Poisonous Toad",
        "It looks a bit like jelly mixed with snail slime",
        Weapon(
            "slime",
            "poisonous goo that makes you shudder uncontrollably",
            randint(1, 3)),
        randint(20, 30))


def generate_cerberus():
    return Enemy(
        "Cerberus",
        "A multi-headed dog that guards the gates of the underworld, "
        + "it is fierce and has knotted fur",
        Weapon(
            "sharp teeth",
            "razer sharp, capable of cutting through tree trunks",
            randint(1, 5)),
        randint(35, 45))


def generate_banchee():
    return Enemy(
        "Banchee",
        "A terrifying looking, old woman with a high pitched scream",
        Weapon(
            "a scream",
            "skull piercing noise that can make your brain boil",
            randint(1, 10)),
        randint(25, 35))


def weight_on_difficulty(number):
    print("Weight on difficulty")
    Logger.instance().debug("calculating creature score based on difficulty")
# TODO validate input
    if ConfigurationLoader.instance().get_difficulty() == Difficulty.EASY:
        return number
    elif ConfigurationLoader.instance().get_difficulty() == Difficulty.MEDIUM:
        number = number + randint(5, 9)
    else:
        number = number + randint(9, 15)
