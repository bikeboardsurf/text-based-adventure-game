from distutils.log import info
from logistics.configurationloader import ConfigurationLoader
from logistics.customenums.difficultyenum import Difficulty
from modules.weapon import Weapon
from modules.carryitem import CarryItem
from modules.enemy import Enemy
from modules.villager import Villager
from modules.magicbeing import MagicBeing
from modules.mapsquareobject import MapSquareObject
from modules.mapsquare import MapSquare
from modules.player import Player
from modules.protection import Protection
from modules.healthpotion import HealthPotion
from logistics.logger import Logger
import constants
import logistics.subgames
import logistics.generateenemies
import utilities
import asciiart
import logistics.subgames
import time
import random
import logging
import copy
from termcolor import colored


class Game:
    def __init__(self):
        self.x = 0
        self.y = 0
        self.grid = []
        self.player = None
        self.bigBadBoss = None
        self.gameRunning = True

    def generate_hero(self):
        Logger.instance().debug("Generating hero")
        player_smells = [
            "mouldy cheese",
            "well worn socks",
            "the bits in between the feet of an ogre",
            "the undergarments of a sickly giant",
            "the underbelly of an ox",
            "the toilet of a herd of ogres"]
        name = utilities.user_input("What is your name warrior? ", True)
        health = random.randint(5, 20)
        Logger.instance().debug("Health is " + str(health))
        description = (
            "You look strong and in need of a bath. " +
            "You smell like " + random.choice(player_smells)
        )
        Logger.instance().debug("Description is " + description)
        player = Player(name, description, health, None, None)
        Logger.instance().debug("Player has been created")
        return player

    def setup_grid(self):
        Logger.instance().debug("Setting up game grid")
        localgrid = []
        enemies = [
            logistics.generateenemies.generate_kraken(),
            logistics.generateenemies.generate_ogre(),
            logistics.generateenemies.generate_cerberus(),
            logistics.generateenemies.generate_banchee(),
            logistics.generateenemies.generate_toad()
           ]
        random.shuffle(enemies)
        villager1 = Villager(
            "Sue Twinkle Toes",
            "has sparkly toes",
            "Hello hero! I hope you can help save us, " +
            "the creature has ruined us")
        villager2 = Villager(
            "Harold the hairy",
            "has hair everywhere, even on his finger nails",
            "Good day tough warrior, you MUST help us to survive, "
            + "give us light at a time of darkness")
        villager3 = Villager(
            "Tommy the tall",
            "looks very big",
            "Welcome friend, please help us")
        villager4 = Villager(
            "Brilla Rigaud",
            "looks like she has not slept in a month",
            "Friend it is good to see you, "
            "only you can defeat the evil lurking in these " +
            "lands")
        villagers = [
                 villager1,
                 villager2,
                 villager3,
                 villager4
                ]
        random.shuffle(villagers)
        wizardWeaponGiver = MagicBeing(
            "Wittler The Weapon Giver",
            "has a very crooked nose",
            "Greetings, hero. You may need a weapon, " +
            "I can provide one for you, just beat me in this game",
            "a wizard")
        witchProtector = MagicBeing(
            "Thistles the Protector",
            "has pale green skin, a long nose and purple boots",
            "Greetings young hero, " +
            "pass this challenge and I shall provide you with " +
            "some protection",
            "a witch")
        wizardHealer = MagicBeing(
            "Haron the Healer",
            "very tall features and is skinny as a rake",
            "You are the chosen one to rid this land of evil, " +
            "complete the quest and I shall give you increased health points",
            "a wizard")
        healthPotion = HealthPotion(
            "a health potion",
            "An elixir devised by the wisest collection of Wizards",
            random.randint(5, 15))
        healerMq = MapSquare(
            "You see a stall of potions",
            "You see a large number of potions in bottles of many shapes, "
            + "sizes and colours")
        healerMq.set_being(wizardHealer)
        healerMq.set_item(healthPotion)
        healerMq.set_callback(logistics.subgames.stone_paper_paper_scissors)
        healerMq.set_locked(True)
        healerMq.set_locked_description("You see a small house, the door is locked, it requires a key")
        healerMq.set_key_required_to_unlock("Golden Key")

        weapons = [
                Weapon(
                    "a sword",
                    "a shiny, sharp weapon probably forged " +
                    "by King Arthur's own blacksmith", 5),
                Weapon(
                    "an axe",
                    "a weapon with two blades",
                    4),
                Weapon(
                    "pompoms",
                    "fluffy, red balls used for cheering on " +
                    "knights during battle",
                    1)
              ]
        helmet = Protection(
            "a helmet",
            "a spikey metal device that fits comfortably on your head",
            2)
        shield = Protection(
            "a shield",
            "a large circular disc with a handle",
            3)
        armourSuit = Protection(
            "armour",
            "a metal suit that makes even the slightest warrior look "
            + "formidable",
            5)
        lazySuzan = Protection(
            "a lazy suzan",
            "a circular device to place food options on, " +
            "not suitable for protection",
            1)

        protectiveItems = [
            helmet,
            shield,
            armourSuit,
            lazySuzan
        ]
        random.shuffle(weapons)
        random.shuffle(protectiveItems)
        random.shuffle(enemies)
        self.bigBadBoss = enemies[0]

        row1 = []
        # A1 in grid
        ms1 = MapSquare(
            "You are in a field next to a wooden hut",
            "you are in a field, next to you is a wooden hut " +
            "with a thatched roof")
        ms1.set_being(villagers[0])
        # B1 in grid
        ms2 = MapSquare(
            "You are in a field of grass",
            "You are in a field of grass, swishy swashy, swishy swoshy")
        ms2.set_item(weapons[0])
        ms2.set_being(wizardWeaponGiver)
        ms2.set_callback(logistics.subgames.maths)
        witchmq = MapSquare(
            "You see a house with smoke rising " +
            "from the chimney, there is a couldron outside",
            "You see a witches house")
        witchmq.set_item(protectiveItems[0])
        witchmq.set_being(witchProtector)
        witchmq.set_callback(logistics.subgames.riddle)
        bossMq = MapSquare(
            "You see a ruined landscape",
            "You can see burnt, perished crops no sign of people. " +
            "You can hear the breathing of a creature behind you, " +
            "you sense evil")
        bossMq.set_being(self.bigBadBoss)
        villageMq = MapSquare(
            "You in a farmers field",
            "You are in a farmers field, the soil has been ploughed " +
            "ready for sowing seeds")
        villageMq.set_being(villagers[1])

        villageMq2 = MapSquare(
            "You are in a muddy river, the water is low",
            "You are in a muddy river, its looking a little low")
        villageMq2.set_being(villagers[2])
        row1 = [
            ms1,
            ms2,
            MapSquare(
                "You are in the centre of a field of grass",
                "You are in the centre of a field of grass, " +
                "the grass is long an itchy"),
            MapSquare(
                "You are in a field of grass, it smells fresh",
                "You are in a field of grass, it smells grassy"),
            MapSquare(
                "You are in a field of grass",
                "You are in a field of grass, swishy swashy, swishy swoshy")
           ]
        row2 = []
        row2 = [
            MapSquare(
                "You are in a muddy river",
                "You are in a muddy river, squelch, squerch, you see " +
                "someone to the east"),
            witchmq,
            MapSquare(
                "You are in a muddy, smelly river.",
                "You are in a muddy river, it smells stale."),
            MapSquare(
                "You are in a muddy river, the mud is sticky",
                "You are in a muddy river, the water is shallow, the " +
                "river and the mud is very sticky"),
            villageMq2
           ]
        row3 = []
        row3 = [
            MapSquare(
                "You are at the edge of a forest",
                "You have entered a forest, the trees are tall and let " +
                "little light in. " +
                "You sense there even more darkness in the south"),
            MapSquare(
                "You are in the forest",
                "You are in an ancient forest, you sense eyes watching " +
                "you from the south"),
            MapSquare(
                "You are deep in the forest",
                "You are in the centre of the forest, you smell " +
                "pine and oak. " +
                "You detect a faint smell of decay coming from the south"),
            villageMq,
            MapSquare(
                "You are at the edge of the forest, " +
                "there are small trees at the edge",
                "You are at the edge of the forest where " +
                "sunlight meets darkness")
           ]
        row4 = []
        row4 = [
            MapSquare(
                "You are in the ruins of a village",
                "You are in what used to be a village, " +
                "there are still smouldering " +
                "embers, evil was here recently"),
            MapSquare(
                "You are in the ruins of a burning village",
                "You are next to an old dried up well of a burning village. " +
                "A path to decay leads to the south"),
            MapSquare(
                "You are in the middle of a village",
                "You are in the middle of a village " +
                "that was thriving but now " +
                "lies ruined."),
            healerMq,
            MapSquare(
                "You are in a village, its very quiet",
                "You are in a village it has recently been " +
                "on fire, its quiet " +
                "apart from small fires")
           ]
        row5 = []
        row5 = [
            MapSquare(
                "You are on a muddy path, evil is close, perhaps to the east",
                "You see no plants around you, the path ahead is muddy, " +
                "you sense evil is close by, perhaps to the east. " +
                "Prepare yourself"),
            bossMq,
            MapSquare(
                "You are on a muddy path, evil is close",
                "You see no plants around you, the path ahead is muddy, " +
                "you sense " +
                "evil is close, perhaps to the west. Prepare yourself"),
            MapSquare(
                "You are on a path, there is not much here",
                "You are on a path, there is little greenery, " +
                "evil has been here, " +
                "tracks suggest it is to the west."),
            MapSquare(
                "You are on a path, there is not much here",
                "You are on a path, there is little greenery, " +
                "evil has been here, " +
                "tracks suggest it is to the west.")
        ]
        localgrid.append(row1)
        localgrid.append(row2)
        localgrid.append(row3)
        localgrid.append(row4)
        localgrid.append(row5)
        self.grid = localgrid
        Logger.instance().debug("The row count of the map grid is "+str(len(self.grid)))
        #To get the number of columns in the grid pick a row and count the columns in there (column count)
        #for all rows should be universal, so it does not matter which row is picked,
        Logger.instance().debug("The column count of the map grid is "+str(len(self.grid[0])))


    def print_map(self):
        utilities.clear_screen()
        map_colour_visited='light_magenta'
        map_colour_not_visited='light_red'
        map_color_current_position='yellow'
        player_current_position_x=self.x
        player_current_position_y=self.y
        row_counter=0
        for row in self.grid:
            column_counter=0
            for column in self.grid[row_counter]:
                if player_current_position_x==column_counter and player_current_position_y==row_counter:
                    print(colored("  O  ", map_color_current_position),end='')
                else:
                    mapsquare = self.grid[row_counter][column_counter]
                    if mapsquare.get_visited():
                        print(colored("  v  ",map_colour_visited),end='')
                    else:
                        print(colored("  ?  ",map_colour_not_visited),end='')
                column_counter=column_counter+1
            print("\n")
            row_counter=row_counter+1
        print("_________________")
        print(colored("O", map_color_current_position),end='')
        print("=current position")
        print(colored("v", map_colour_visited),end='')
        print("=visited this part of the map")
        print(colored("?", map_colour_not_visited),end='')
        print("=unknown territory")

        print("_________________")

    def print_inventory(self):
        utilities.clear_screen()
        output_info = "You are carrying no weapon"
        if self.player.get_weapon():
            output_info = "As a weapon you are carrying: "+str(self.player.get_weapon())
        print(colored(output_info, 'red'))
        output_info = "you are carrying no protection"
        if self.player.get_protection():
            output_info =" For projection you are carrying: "+str(self.player.get_protection())
        print(colored(output_info, 'green'))
        output_info = "you are carrying no healing potion"
        if self.player.get_health_potion():
            output_info = "For healing you are carrying: "+str(self.player.get_health_potion())
        print(colored(output_info, "light_yellow"))
        output_info = "You are carrying no other items\n"
        if self.player.get_carry_items():
            output_info = "You have the following in your pockets:\n"
            items=""
            for carry_item in self.player.get_carry_items():
                items += carry_item.get_name()+"\n"
            output_info+=items
        print(output_info)

    def boss_fight(self):
        utilities.clear_screen()
        asciiart.fight()
        hasDrunkHealthPotion = False

        time.sleep(2)
        print(
            "Player weapon info:\nYou are carrying ",end='') 
        print(
            colored(str(self.player.get_weapon())+"\n", 'green'))
        print(
            "Player protection:\nYou are carrying ",end='')
        print(
            colored(str(self.player.get_protection())+"\n\n",'green'))
        print(str(self.bigBadBoss))
        utilities.user_input("Press enter to continue..", False)
        print("")
        hasWeapon = self.player.get_weapon() is not None
        hasProtection = self.player.get_protection() is not None
        Logger.instance().debug(
            "Does the player have a weapon? "+str(hasWeapon))
        Logger.instance().debug(
            "Does the player have protection? "+str(hasProtection))    
        while self.player.get_health() > 0:
            Logger.instance().debug(
            "Health is "+str(self.player.get_health()))
            hit = random.randint(0, 5)
            if hasWeapon and hit > 0:
                Logger.instance().debug(
                    "Has a weapon and hit was above 0")
                hit = hit + self.player.get_weapon().get_hit_points()
                if hit < 2:
                    # Some descriptive excuses by the player
                    # did badly in the fight
                    outcomes = (
                        ConfigurationLoader.instance()
                        .get_bad_fighting_outcomes())
                    print(
                        "You "+outcomes[random.randint(0, len(outcomes)-1)] +
                        " and only manage ", end='')
                    print(colored(str(hit) + " damage", 'green'))
                elif hit > 4:
                    goodoutcomes = (
                        ConfigurationLoader.instance()
                        .get_good_fighting_outcomes())
                    print(
                        "You " +
                        goodoutcomes[
                            random.randint(0, len(goodoutcomes) - 1)] +
                        " to cause ",end='')
                    print(
                        colored(str(hit)+ " damage",'green')
                    )
                else:
                    print(
                        "You use your weapon and cause ",end='') 
                    print(
                        colored(str(hit)+" damage",'green'))
                self.bigBadBoss.deduct_health(hit)
            else:
                if hasWeapon:
                    print(
                        "You use your weapon and embarrassingly " +
                        "miss the creature")
                else:
                    print(
                        "You have no weapon, you dance around " +
                        "awkwardly hoping to distract the creature")

            if self.bigBadBoss.get_health() < 1:
                break
            print(
                "Your enemy has " +
                str(self.bigBadBoss.get_health()) +
                " health.")
            enemyhit = random.randint(0, 5)
            if (
                hasProtection and
                self.player.get_protection().get_protection_points() > 0
                and enemyhit >= self.player.get_protection()
                .get_protection_points() and
                    self.player.get_health() < 10):
                Logger.instance().debug("Protection enabled")
                enemyhit = (
                    enemyhit - self.player.get_protection()
                    .get_protection_points())
                print(
                    "The "+self.bigBadBoss.get_name() +
                    " uses its " + self.bigBadBoss.get_weapon().get_name() +
                    " on you but your " +
                    self.player.get_protection().get_name() +
                    " buffers the blow, the damage is reduced by " +
                    str(self.player.get_protection()) +
                    " to " +
                    str(enemyhit))
                self.player.deduct_health(enemyhit)
            else:
                print(
                    "The " + self.bigBadBoss.get_name() +
                    " attacks and causes ")
                print(colored(str(enemyhit) +
                    " damage", 'red'))
                self.player.deduct_health(enemyhit)
            if (
                self.player.get_health() < 
                    constants.HEALTH_THRESHOLD_WHEN_PROTECTION_IS_ENABLED and
                    self.player.get_health_potion() is not None and
                    hasDrunkHealthPotion is False):
                Logger.instance().debug("Health potion activated")    
                addedHealthPoints = (
                    self.player.get_health_potion()
                    .get_health_points())
                
                print(
                    colored("You remember your health potion and take it quickly, " +
                    "it adds " + str(addedHealthPoints), 'green'))
                self.player.add_health(addedHealthPoints)
                hasDrunkHealthPotion = True
                print(
                    colored("Your health is now " +
                    str(self.player.get_health()), 'green'))
            if self.player.get_health() < 1:
                break
            print(
                "You now have " +
                str(self.player.get_health()) + " health")
            print("")
            utilities.user_input("Press enter to continue", False)
            utilities.clear_screen()

        if self.player.get_health() < 1:
            print(
                colored(
                "Unfortunately you did not make it this time. " +
                "It's  GAME OVER for you, better luck next time", 'red'))
            asciiart.skeleton()
        else:
            asciiart.unicorn()
            print(
                colored(
                "You beat the terrible beast CONGRATULATIONS " +
                "you won the big battle. Villagers erect a " +
                "large statue of " , 'light_yellow'),end='')
            print(
                colored(
                    self.player.get_name(), 'light_blue'),end='')
            print(
                colored(
                    " and call their monsterless land " +
                self.player.get_name(),'light_yellow')
            )
            print(
                "You have unlocked a secret code of ",end='')
            print(
                colored(
                  ConfigurationLoader.instance().get_successful_code(), 'green')
            )

    def choose_direction(self):
        print(
            "Which direction would you like to go (or press m for map or i for inventory)?\n")
        # The highest North is the first row of the 2D array
        if self.y == 0:
            print(
                "You cannot move any further north owing to the perilous " +
                "mountains to the north")
        else:
            print("n for north")
        # get a row from the grid - they should all be of
        # equal length so just pick one
        rowItem = self.grid[1]
        maxEast = len(rowItem)
        if self.x < maxEast - 1:
            print("e for east")
        else:
            print(
                "You cannot go any further east owing to the ferocious " +
                "river rapids willing to sweep you away")
        if self.y < len(self.grid) - 1:
            print("s for south")
        else:
            print(
                "You cannot move any further south owing to the bogs of " +
                "doom")
        if self.x > 0:
            print("w for west")
        else:
            print(
                "You are unable to go any further west, the shadows are " +
                "dark and the ice is unbearable")
        print("")
        choice = utilities.user_input("", True)
        return choice

    def call_action(self, callback):
        return callback()

    def start_game(self):
        utilities.clear_screen()
        asciiart.title()
        asciiart.castle()
        print()
        print()
        self.setup_grid()
        self.player = self.generate_hero()
        print(str(self.player))
        Logger.instance().debug("Loading configuration")

        if(ConfigurationLoader.instance().god_mode()):
            Logger.instance().info("You are running in GOD MODE, be sensible")
            self.player.set_god_mode(True)
        else:
            Logger.instance().debug("You are NOT running in GOD MODE")

        if (ConfigurationLoader.instance().get_difficulty() == Difficulty.HARD):
            Logger.instance().info("LEVEL IS HARD")

        print()
        print()
        utilities.user_input("Press enter key to continue", False)
        utilities.clear_screen()
        mapsquare = self.grid[self.y][self.x]
        mapsquare.set_visited(True)
        print(str(mapsquare))
        while self.gameRunning:
            move = self.choose_direction()
            if move == "m":
                self.print_map()
            elif move == "i":
                self.print_inventory()
            else:
                self.move_player(move)

    def convert_alphabet_to_numerical(self, character):
        return ord(character) - 96

    def move_player(self, character):
        utilities.clear_screen()
        if character == "n":
            if self.y == 0:
                print(
                    "You cannot move any further north owing " +
                    "to the perilous mountains to the north")
            else:
                self.y = self.y - 1
        elif character == "s":
            if self.y < len(self.grid) - 1:
                self.y = self.y + 1
            else:
                print(
                    "You cannot move any further south owing to " +
                    "the bogs of doom")
        elif character == "e":
            # get a row from the grid - they should all be of equal
            # length so just pick one
            rowItem = self.grid[1]
            maxEast = len(rowItem)
            # logging.debug("Max east is "+str(maxEast))
            if self.x < maxEast - 1:
                self.x = self.x + 1
            else:
                print(
                    "You cannot go any further east owing to the ferocious " +
                    "river rapids willing to sweep you away")
        elif character == "w":
            if self.x > 0:
                self.x = self.x - 1
            else:
                print(
                    "You are unable to go any further west, the shadows " +
                    "are dark and the ice is unbearable")
        mapsquare = self.grid[self.y][self.x]
        print(str(mapsquare))
        mapsquare.set_visited(True)
        if mapsquare.is_locked():
            Logger.instance().debug(
                "MapSqaure is locked"
            )
            print(colored(
                "You cannot pass, you need some kind of key. "+
                    mapsquare.get_locked_description(), 'red')
                    )
        # Dont like the below, if rename method will break,
        # perhaps use Exceptions instead
        if(
            hasattr(mapsquare, "callback") and
                callable(mapsquare.get_callback())):
            # logging.debug("mapsquare has callback")
            enter_challenge = utilities.user_input(
                "Would you like to engage (y for yes)?\n", True).lower()
            if enter_challenge == "y":
                if(
                    self.player.get_god_mode() or
                        self.call_action(mapsquare.get_callback())):
                    msItem = mapsquare.get_item()
                    if(msItem is not None):
                        clonedItem = copy.deepcopy(msItem)
                        if(isinstance(msItem, Weapon)):
                            Logger.instance().debug(
                                "Player has won a weapon")
                            self.player.set_weapon(clonedItem)
                            print(colored(
                                    "You have just won " +
                                    str(self.player.get_weapon()), 'light_green'))
                            self.reset_square(mapsquare)
                        elif(isinstance(msItem, Protection)):
                            Logger.instance().debug(
                                "Player has won a " + "protective item")
                            self.player.set_protection(clonedItem)
                            print(colored(
                                    "You have won " +
                                    str(self.player.get_protection()),'light_green'))
                            self.reset_square(mapsquare)
                        elif(isinstance(msItem, HealthPotion)):
                            Logger.instance().debug(
                                "Player has won a health potion")
                            self.player.set_health_potion(clonedItem)
                            print(colored(
                                "You have won " +
                                str(self.player.get_health_potion()),'light_green'))
                            self.reset_square(mapsquare)
                else:
                    Logger.instance().debug(
                        "Player failed to win dual")
                    print(
                        colored("You failed to win the dual",
                                 'red'))
            else:
                Logger.instance().debug(
                    "Player did not engage with sprite")
        else:
            if(isinstance(mapsquare.get_being(), Enemy)):
                Logger.instance().debug(
                    "Player has found the big boss")
                enter_challenge = utilities.user_input(
                    "Would you like to fight (y for yes)?\n", True).lower()
                if enter_challenge == "y":
                    self.boss_fight()
                    self.gameRunning = False
                else:
                    self.player.deduct_health(1)
                    print(
                        "You run away you get injured in the process " +
                        "your health is now " +
                        str(self.player.get_health()))
                    if self.player.get_health() < 1:
                        print(
                            "You have succumbed to your injuries, " +
                            "you cannot continue. " +
                            "GAME OVER")
                        asciiart.skeleton()
                        self.gameRunning = False

    def reset_square(self, mapsquare):
        mapsquare.set_item(None)
        mapsquare.set_being(None)
        mapsquare.set_callback(None)

   
