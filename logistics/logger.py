import json
import os
import logging
from logistics.customenums.logginglevelenum import LoggingLevel
import logging


class Logger(object):
    _instance = None
    __nativeLogger = logging.getLogger("game")

    def __init__(self):
        raise RuntimeError('Call instance() instead')

    @classmethod
    def instance(cls):
        if cls._instance is None:
            cls._instance = cls.__new__(cls)
            # Put any initialization here.
            path = os.path.join("configuration", "configuration.json")
            with open(path) as f:
                loggerconfig = json.load(f)["LOGGING"]
                cls.__nativeLogger.setLevel(loggerconfig["LEVEL"])
        return cls._instance

    def debug(self, str):
        Logger.instance().__nativeLogger.debug(str)

    def info(self, str):
        Logger.instance().__nativeLogger.info(str)

    def warn(self, str):
        Logger.instance().__nativeLogger.warn(str)

    def error(self, str):
        Logger.instance().__nativeLogger.error(str)

    def exception(self, str):
        Logger.instance().__nativeLogger.exception(str)

    def critical(self, str):
        Logger.instance().__nativeLogger.critical(str)
