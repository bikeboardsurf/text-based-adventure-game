# What the Gang of Four’s original Singleton Pattern
# might look like in Python.
import json
import os
from warnings import catch_warnings
from xmlrpc.client import Boolean
from logistics.customenums.difficultyenum import Difficulty
from customexceptions.configurationexception import ConfigurationException
import utilities


class ConfigurationLoader(object):
    _instance = None
    __riddles = None
    __difficulty = None
    __good_fighting_outcomes = None
    __bad_fighting_outcomes = None
    __god_mode = False
    __successful_code = None
    __maths_timer = None
    __maths_maximum_Numer = None
    _unlock_keys = None

    def __init__(self):
        raise RuntimeError('Call instance() instead')

    @classmethod
    def instance(cls):
        if cls._instance is None:
            cls._instance = cls.__new__(cls)
            # Put any initialization here.
            try:
                path = os.path.join(
                    "configuration", "game-configuration.json")
                with open(path) as f:
                    cls.__game_settings = json.load(f)
                    cls.__difficulty = Difficulty(
                        cls.__game_settings["DIFFICULTY"])
                    cls.__good_fighting_outcomes = (
                        cls.__game_settings["GOOD-FIGHTING-OUTCOMES"])
                    cls.__bad_fighting_outcomes = (
                        cls.__game_settings["BAD-FIGHTING-OUTCOMES"])
                    tmp_successful_code = (
                        cls.__game_settings["SUCCESSFUL-CODE"])
                    tmp_godmodeFromConfig = cls.__game_settings["GODMODE"]
                    if (
                        utilities.is_null_or_is_empty_if_trimmed(
                            tmp_godmodeFromConfig)):
                        raise ConfigurationException(
                            "The value for god mode in the " +
                            "configuration file " +
                            "appears to be blank, it must be True or False")
                    if tmp_godmodeFromConfig.strip().lower() == "true":
                        cls.__god_mode = True
                    if utilities.is_null_or_is_empty_if_trimmed(
                            tmp_successful_code):
                        raise ConfigurationException(
                            "The value " +
                            "for the successful code in " +
                            "the configuration " +
                            "file appears to be blank. " +
                            "It must have a value")
                    else:
                        cls.__successful_code = tmp_successful_code.strip()
            except (FileNotFoundError, FileExistsError) as err:
                raise ConfigurationException(
                    "Could not load game configuration file, " +
                    "check if configuration file exists")
            try:
                path = os.path.join(
                    "configuration", "unlock-keys.json")
                with open(path) as f:
                    cls._unlock_keys = json.load(f)
            except (FileNotFoundError, FileExistsError) as err:
                raise ConfigurationException(
                    "Could not load unlock key configuration file, " +
                    "check if configuration file exists")
            try:
                path = os.path.join(
                    "configuration", "riddles-configuration.json")
                with open(path) as f:
                    cls.__riddles = json.load(f)[cls.__difficulty.value]
            except (FileNotFoundError, FileExistsError) as err:
                raise ConfigurationException(
                    "Could not load riddle configuration file, " +
                    "check if configuration file exists")

            try:
                path = os.path.join(
                    "configuration", "mathematics-configuration.json")
                with open(path) as f:
                    cls.__maths_settings = json.load(f)
                    cls.__maths_timer = (
                        (cls.__maths_settings["TIMER"])
                        [cls.__difficulty.value])
                    cls.__maths_max_num = (
                        (cls.__maths_settings["MAX_NUM"])
                        [cls.__difficulty.value])
            except (FileNotFoundError, FileExistsError) as err:
                raise ConfigurationException(
                    "Could not load maths configuration file, " +
                    "check if configuration file exists and it is correct")


        return cls._instance

    def get_maths_timer(self):
        return ConfigurationLoader.instance().__maths_timer
    
    def get_maths_max_numer(self):
        return ConfigurationLoader.instance().__maths_max_num

    def get_riddles(self):
        return ConfigurationLoader.instance().__riddles

    def get_game_settings(self):
        return ConfigurationLoader.instance().__game_settings

    def get_good_fighting_outcomes(self):
        return ConfigurationLoader.instance().__good_fighting_outcomes

    def get_bad_fighting_outcomes(self):
        return ConfigurationLoader.instance().__bad_fighting_outcomes

    def get_difficulty(self):
        return ConfigurationLoader.instance().__difficulty

    def god_mode(self):
        return ConfigurationLoader.instance().__god_mode

    def get_successful_code(self):
        return ConfigurationLoader.instance().__successful_code
    
    def get_unlock_keys(self):
        return ConfigurationLoader.instance()._unlock_keys
