import os
import sys
from logistics.logger import Logger


# A static 'class'
# Simple function that clears the screen
def clear_screen():
    os.system("cls" if os.name == "nt" else "clear")


# A method that gracefully handles user input
# and handles keyboard interrupts
def user_input(instruction, mustNotBeBlank):
    try:
        Logger.instance().debug("checking input raw input " + instruction)
        userIn = input(instruction)
        Logger.instance().debug("UserIn is " + userIn)
        if (mustNotBeBlank):
            Logger.instance().debug("Must not be blank?" + str(mustNotBeBlank))
            while (len(userIn.strip()) == 0):
                Logger.instance().debug("Input needs to have a value and is blank")
                print("Your input must not be blank")
                userIn = input(instruction)
                print("USer in is " + userIn)
        Logger.instance().debug("Returning user input of " + userIn)
        return userIn
    except KeyboardInterrupt:
        Logger.instance().critical("Keyboard Interrupt has been issued")
        print("Exiting game")
        try:
            sys.exit(0)
        except SystemExit:
            os._exit(0)


def is_null_or_is_empty_if_trimmed(input_str):
    """Returns true if null or if empty (even if trimmed at start and end)"""
    if not input_str:
        return True
    
    if not input_str.strip():
        return True
    else:
        return False
